#Enjoy代码生成器
#Enjoy
谁用谁知道~
运行流程:
1, 利用模版引擎 拼接 好字符串文本
2, 文本写入到代码那个文件夹下面 就可以了

![结构介绍](https://git.oschina.net/uploads/images/2017/0808/153343_1c89a595_619050.png "tpp1.png")


测试生成一套~
```

public static void main(String[] args) {
        String className = "Test";
        String tableName = "t_test";

        JfGenerator.me
        .setSrcFolder("src/main/java")
        .setViewFolder("src/main/webapp")
        .setPackageBase("com")
        .setBasePath("demo")
        .tableSql(getSqlList())
        .javaRender(className, tableName)
        .htmlRender(className, tableName);

        System.out.println("---------OK-刷新一下项目吧---------");
    }
    
    private static List<String> getSqlList() {
        ArrayList<String> sqlList = new ArrayList<String>();
        
        sqlList.add("DROP TABLE IF EXISTS `t_test`;");
        sqlList.add("CREATE TABLE `t_test` ( " +
                "  `id` int(11) NOT NULL AUTO_INCREMENT, " +
                "  `test` varchar(255) DEFAULT NULL, " +
                "  PRIMARY KEY (`id`) " +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        return sqlList;
    }

```
